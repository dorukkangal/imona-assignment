package com.imona.assignment.service;

import com.imona.assignment.model.Customer;

public interface CustomerService extends GenericEntityService<Customer>
{

}
