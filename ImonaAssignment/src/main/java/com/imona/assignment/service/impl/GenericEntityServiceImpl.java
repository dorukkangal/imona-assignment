package com.imona.assignment.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.imona.assignment.dao.GenericEntityDao;
import com.imona.assignment.model.Customer;
import com.imona.assignment.service.GenericEntityService;

@Service
public class GenericEntityServiceImpl<T extends Customer> implements GenericEntityService<T>
{
	@Autowired
	private GenericEntityDao<T> entityDao;

	@Transactional
	public void delete( T entity )
	{
		entityDao.delete( entity );
	}

	@Transactional
	public T get( Integer id )
	{
		return entityDao.get( id );
	}

	@Transactional
	public Collection<T> getAll()
	{
		return entityDao.getAll();
	}

	@Transactional
	public void save( T entity )
	{
		entityDao.save( entity );
	}

	@Transactional
	public void update( T entity )
	{
		entityDao.update( entity );
	}

}
