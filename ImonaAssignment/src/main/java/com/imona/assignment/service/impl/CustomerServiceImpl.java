package com.imona.assignment.service.impl;

import org.springframework.stereotype.Service;

import com.imona.assignment.model.Customer;
import com.imona.assignment.service.CustomerService;

@Service
public class CustomerServiceImpl  extends GenericEntityServiceImpl<Customer> implements CustomerService
{

}
