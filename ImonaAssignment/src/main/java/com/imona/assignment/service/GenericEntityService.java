package com.imona.assignment.service;

import java.util.Collection;

public interface GenericEntityService<T>
{
	Collection<T> getAll();

	T get( Integer id );

	void save( T item );

	void update( T item );

	void delete( T item );
}
