package com.imona.assignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.imona.assignment.service.CustomerService;
import com.imona.assignment.ui.CustomerForm;
import com.imona.assignment.ui.CustomerTable;
import com.vaadin.Application;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window;

@SuppressWarnings( "serial" )
@Configurable
public class ImonaAssignmentApplication extends Application
{
	@Autowired
	CustomerService	customerService;

	private Window mainWindow;
	private CustomerForm registerForm;
	private CustomerTable customerTable;
	private Button show = new Button( "Show" );

	@Override
	public void init()
	{
		mainWindow = new Window( "Imona Assignment" );

		customerTable = new CustomerTable( this );
		registerForm = new CustomerForm( this );
		customerTable.setVisible( false );

		refreshPage();

		show.addListener( new Button.ClickListener()
		{
			public void buttonClick( ClickEvent event )
			{
				customerTable.setVisible( true );
				show.setVisible( false );
			}
		} );
		setMainWindow( mainWindow );
	}

	public void refreshPage()
	{
		mainWindow.removeAllComponents();
		customerTable.refresh();
		mainWindow.addComponent( registerForm );
		mainWindow.addComponent( customerTable );
		mainWindow.addComponent( show );
	}

	public CustomerService getCustomerService()
	{
		return customerService;
	}

}
