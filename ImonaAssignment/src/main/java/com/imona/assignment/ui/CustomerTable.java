package com.imona.assignment.ui;

import java.util.List;

import com.imona.assignment.ImonaAssignmentApplication;
import com.imona.assignment.model.Customer;
import com.imona.assignment.service.CustomerService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnResizeEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings( "serial" )
public class CustomerTable extends CustomComponent
{
	private CustomerService customerService;
	private Table customerTable;
	private List<Customer> customerList;
	private Customer customer;

	private VerticalLayout mainLayout;

	public CustomerTable( ImonaAssignmentApplication application )
	{
		customerService = application.getCustomerService();
		buildMainLayout();
		setCompositionRoot( mainLayout );
	}

	private void buildMainLayout()
	{
		mainLayout = new VerticalLayout();

		customerTable = new Table( "Customer Table" );
		customerTable.addContainerProperty( "Name", String.class, "" );
		customerTable.addContainerProperty( "Surname", String.class, "" );
		customerTable.addContainerProperty( "Gender", String.class, "" );
		customerTable.addContainerProperty( "Details", Button.class, null );

		refresh();

		customerTable.addListener( new Table.ColumnResizeListener()
		{
			@Override
			public void columnResize( ColumnResizeEvent event )
			{

			}
		} );
		customerTable.setImmediate( true );
		customerTable.setPageLength( customerList.size() );
		customerTable.setColumnCollapsingAllowed( true );

		mainLayout.addComponent( customerTable );
	}

	public void refresh()
	{
		customerList = (List<Customer>) customerService.getAll();
		
		customerTable.removeAllItems();
		for ( int i = 0; i < customerList.size(); i++ )
		{
			Integer itemId = new Integer( i );
			customer = customerList.get( i );

			Button detailsField = new Button( "Search" );
			detailsField.setData( itemId );
			detailsField.addListener( new Button.ClickListener()
			{
				public void buttonClick( ClickEvent event )
				{
					Integer itemId = (Integer) event.getButton().getData();
					customer = customerList.get( itemId.intValue() );

					final Window detailWindow = new Window( "Details about Customer" );
					detailWindow.setClosable( true );
					detailWindow.setWidth( "300px" );
					detailWindow.center();

					final CustomerForm customerForm = new CustomerForm();
					customerForm.getField( "name" ).setValue( customer.getName() );
					customerForm.getField( "surname" ).setValue( customer.getSurname() );
					detailWindow.addComponent( customerForm );

					detailWindow.addComponent( new Button( "Update", new Button.ClickListener()
					{
						public void buttonClick( ClickEvent event )
						{
							customer.setName( (String) customerForm.getField( "name" ).getValue() );
							customer.setSurname( (String) customerForm.getField( "surname" ).getValue() );
							customerService.update( customer );
							customerForm.refresh();
							detailWindow.setVisible( false );
							getWindow().showNotification( "Successfully completed" );
							refresh();
						}
					} ) );
					detailWindow.addComponent( new Button( "Delete", new Button.ClickListener()
					{
						public void buttonClick( ClickEvent event )
						{
							customerService.delete( customer );
							getWindow().showNotification( "Successfully completed" );
							customerForm.refresh();
							detailWindow.setVisible( false );
							refresh();
						}
					} ) );
					getWindow().addWindow( detailWindow );
				}
			} );
			detailsField.addStyleName( "link" );
			
			String gender = ( customer.getGender() == true ) ? "Male" : "Female";

			customerTable.addItem( new Object[]
					{
						customer.getName(),
						customer.getSurname(),
						gender,
						detailsField
					}, itemId );
		}
		customerTable.setPageLength( customerList.size() );
	}
}
