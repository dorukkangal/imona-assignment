package com.imona.assignment.ui;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.imona.assignment.ImonaAssignmentApplication;
import com.imona.assignment.model.Customer;
import com.imona.assignment.service.CustomerService;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Form;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings( "serial" )
public class CustomerForm extends Form implements ClickListener
{
	private ImonaAssignmentApplication application;
	private CustomerService customerService;
	private Customer customer;
	private BeanItem<Customer> beanItem;

	private VerticalLayout mainLayout;
	private Button save;

	public CustomerForm( ImonaAssignmentApplication application )
	{
		this.customerService = application.getCustomerService();
		this.application = application;

		save = new Button( "Save", (ClickListener) this );
		refresh();
		mainLayout.addComponent( save );
	}
	
	public CustomerForm()
	{
		refresh();
	}

	public void refresh()
	{
		customer = new Customer();
		beanItem = new BeanItem<Customer>( customer );

		setFormFieldFactory( new CustomerFormFieldFactory() );
		setItemDataSource( beanItem );

		Vector<String> order = new Vector<String>();
		order.add( "name" );
		order.add( "surname" );
		order.add( "gender" );
		setVisibleItemProperties( order );

		Map<String, Boolean> gender = new HashMap<String, Boolean>();
		gender.put( "Male", true );
		gender.put( "Female", false );

		final ComboBox genderBox = new ComboBox( "Gender", gender.keySet() );
		addField( "gender", genderBox );

		genderBox.setInputPrompt("Select a value");
		genderBox.setWidth( "100px" );
		genderBox.setImmediate( true );
		
		genderBox.addListener( new Property.ValueChangeListener()
		{
			@Override
			public void valueChange( com.vaadin.data.Property.ValueChangeEvent event )
			{
				if ( genderBox.getValue() != null )
					customer.setGender( ( genderBox.getValue() == "Male" ) ? true : false );
			}
		} );

		getField( "name" ).setValue( "" );
		getField( "name" ).setRequired( true );
		getField( "name" ).setRequiredError( "Name is missing" );
		getField( "surname" ).setValue( "" );
		getField( "surname" ).setRequired( true );
		getField( "surname" ).setRequiredError( "Surname is missing" );
		genderBox.setValue( null );
		getField( "gender" ).setRequired( false );
		getField( "gender" ).setRequiredError( "Gender is missing" );

		mainLayout = new VerticalLayout();
		mainLayout.setSpacing( true );

		setFooter( mainLayout );
		setCaption( "Customer Form" );
	}

	@Override
	public void buttonClick( ClickEvent event )
	{
		Button button = event.getButton();

		if ( button == save )
		{
			if ( !isValid() )
			{
				getWindow().showNotification( "Form is invalid" );
				return;
			}
			customerService.save( customer );
			refresh();
			getWindow().showNotification( "Successfully completed" );
			application.refreshPage();
			mainLayout.addComponent( save );

			// final Window success = new Window();
			// success.center();
			// success.addComponent( new Label( "Successfully completed" ) );
			// Button close = new Button( "Close" );
			// close.addStyleName( "link" );
			// success.addComponent( close );
			// close.addListener( new Button.ClickListener()
			// {
			// public void buttonClick( ClickEvent event )
			// {
			// success.setVisible( false );
			// }
			// } );
			// getWindow().addWindow( success );
		}
	}
}
