package com.imona.assignment.ui;

import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;

@SuppressWarnings( "serial" )
public class CustomerFormFieldFactory implements FormFieldFactory
{
	@Override
	public Field createField( Item item, Object propertyId, Component uiContext )
	{
		String propertyName = (String) propertyId;

		if ( propertyName.equals( "name" ) )
		{
			return new TextField( "Name" );
		}
		else if ( propertyName.equals( "surname" ) )
		{
			return new TextField( "Surname" );
		}
//		else if ( propertyName.equals( "gender" ) )
//		{
//			Map<String, Boolean> gender = new HashMap<String, Boolean>();
//			gender.put( "Male", true );
//			gender.put( "Female", false );
//
//			final ComboBox genderBox = new ComboBox( "Gender", gender.keySet() );
//
//			genderBox.setInputPrompt("Select a value");
//			genderBox.setWidth( "100px" );
//			genderBox.setImmediate( true );
//
//			genderBox.addItem( Gender.MALE );
//			genderBox.addItem( Gender.FEMALE );
//			genderBox.setValue( Gender.MALE );
//			// OptionGroup gender = new OptionGroup( "Gender" );
//			// gender.addItem( "Male" );
//			// gender.addItem( "Female" );
//
//			return genderBox;
//		}
//
		return null;
	}
}
//enum Gender
//{
//	MALE( "Male", true ),
//	FEMALE( "Female", false );
//
//	private String gender;
//	private Boolean	value;
//
//	private Gender( String gender, Boolean value )
//	{
//		this.gender = gender;
//		this.value = value;
//	}
//
//	public String getGender()
//	{
//		return gender;
//	}
//
//	public Boolean getValue()
//	{
//		return value;
//	}
//}
