package com.imona.assignment.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table( name = "CUSTOMER" )
@Inheritance( strategy = InheritanceType.JOINED )
public class Customer implements Serializable
{
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String surname;
	private Boolean gender;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	public Integer getId()
	{
		return id;
	}

	public void setId( Integer id )
	{
		this.id = id;
	}

	@Column( name = "NAME" )
	public String getName()
	{
		return name;
	}

	public void setName( String name )
	{
		this.name = name;
	}
	
	@Column( name = "SURNAME" )
	public String getSurname()
	{
		return surname;
	}

	public void setSurname( String surname )
	{
		this.surname = surname;
	}

	@Column( name = "GENDER" )
	public Boolean getGender()
	{
		return gender;
	}

	public void setGender( Boolean gender )
	{
		this.gender = gender;
	}

}
