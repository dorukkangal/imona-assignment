package com.imona.assignment.dao;

import com.imona.assignment.model.Customer;

public interface CustomerDao extends GenericEntityDao<Customer>
{
}
