package com.imona.assignment.dao;

import java.util.Collection;

import com.imona.assignment.model.Customer;

public interface GenericEntityDao<T extends Customer>
{
	Collection<T> getAll();

	T get( Integer id );

	void save( T item );

	void update( T item );

	void delete( T item );
}
