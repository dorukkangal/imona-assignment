package com.imona.assignment.dao.impl;

import org.springframework.stereotype.Repository;

import com.imona.assignment.dao.CustomerDao;
import com.imona.assignment.model.Customer;

@Repository
public class CustomerDaoImpl extends GenericEntityDaoImpl<Customer> implements CustomerDao
{
	public CustomerDaoImpl()
	{
		super( Customer.class );
	}
}
