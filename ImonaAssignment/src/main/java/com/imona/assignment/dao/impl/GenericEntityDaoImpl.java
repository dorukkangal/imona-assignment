package com.imona.assignment.dao.impl;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.imona.assignment.dao.GenericEntityDao;
import com.imona.assignment.model.Customer;

@Repository
public abstract class GenericEntityDaoImpl<T extends Customer> implements GenericEntityDao<T>
{
	@Autowired
	private SessionFactory sessionFactory;

	private static final Log LOG = LogFactory.getLog(GenericEntityDaoImpl.class);

	private Class<T> entityClass;

	public GenericEntityDaoImpl( final Class<T> entityClass )
	{
		this.entityClass = entityClass;
	}

	@SuppressWarnings( "unchecked" )
	public Collection<T> getAll()
	{
		LOG.debug( "Get all " + entityClass.getSimpleName() );
		return (Collection<T>) sessionFactory.getCurrentSession().createQuery( "from " + entityClass.getSimpleName() ).list();
//				createCriteria( entityClass );
	}

	@SuppressWarnings( "unchecked" )
	public T get( Integer id )
	{
		LOG.debug( "Get by id " + id + entityClass.getSimpleName() );
		return (T) sessionFactory.getCurrentSession().get( entityClass, id );
	}

	public void save( T item )
	{
		LOG.debug( "Save new item " + entityClass.getSimpleName() );
		sessionFactory.getCurrentSession().save( item );
	}

	public void update( T item )
	{
		LOG.debug( "Update item " + entityClass.getSimpleName() );
		sessionFactory.getCurrentSession().update( item );
	}

	public void delete( T item )
	{
		LOG.debug( "Delete item " + entityClass.getSimpleName() );
		sessionFactory.getCurrentSession().delete( item );
	}

}
